package ar.com.Invest.Invest.dtos;

public class EmpresaDTO {

    private String nombre;
    private String simbolo;

//    Facebook (FB)
//    Apple (AAPL)
//    Microsoft (MSFT)
//    Google (GOOGL)
//    Amazon (AMZN)

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }
}
