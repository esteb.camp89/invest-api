package ar.com.Invest.Invest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DailyDTO {

    @JsonProperty(value = "1. open")
    private Double open;

    @JsonProperty(value = "2. high")
    private Double high;

    @JsonProperty(value = "3. low")
    private Double low;

    @JsonProperty(value = "4. close")
    private Double close;

    @JsonProperty(value = "5. adjusted close")
    private Double adjustedClose;

    @JsonProperty(value = "6. volume")
    private Long volume;

    @JsonProperty(value = "7. dividend amount")
    private Double dividendAmount;

    @JsonProperty(value = "8. split coefficient")
    private Double splitCoefficient;

    public Double getOpen() {
        return open;
    }

    public void setOpen(Double open) {
        this.open = open;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getClose() {
        return close;
    }

    public void setClose(Double close) {
        this.close = close;
    }

    public Double getAdjustedClose() {
        return adjustedClose;
    }

    public void setAdjustedClose(Double adjustedClose) {
        this.adjustedClose = adjustedClose;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    public Double getDividendAmount() {
        return dividendAmount;
    }

    public void setDividendAmount(Double dividendAmount) {
        this.dividendAmount = dividendAmount;
    }

    public Double getSplitCoefficient() {
        return splitCoefficient;
    }

    public void setSplitCoefficient(Double splitCoefficient) {
        this.splitCoefficient = splitCoefficient;
    }

    @Override
    public String toString() {
        return "DailyDTO{" +
                "open=" + open +
                ", high=" + high +
                ", low=" + low +
                ", close=" + close +
                ", adjustedClose=" + adjustedClose +
                ", volume=" + volume +
                ", dividendAmount=" + dividendAmount +
                ", splitCoefficient=" + splitCoefficient +
                '}';
    }
}
