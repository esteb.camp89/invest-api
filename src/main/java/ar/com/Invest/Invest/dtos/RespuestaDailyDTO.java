package ar.com.Invest.Invest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RespuestaDailyDTO {

    private String date;
    private String value;
    private String previous;

    @JsonProperty(value = "change_percent")
    private String changePercent;
    @JsonProperty(value = "change_value")
    private String changeValue;
    @JsonProperty(value = "color_code")
    private String colorCode;

    public String getChangePercent() {
        return changePercent;
    }

    public void setChangePercent(String changePercent) {
        this.changePercent = changePercent;
    }

    public String getChangeValue() {
        return changeValue;
    }

    public void setChangeValue(String changeValue) {
        this.changeValue = changeValue;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "RespuestaDailyDTO{" + "date='" + date + '\'' + ", value='" + value + '\'' + ", previous='" + previous + '\'' + ", changePercent='" + changePercent + '\'' + ", changeValue='" + changeValue + '\'' + ", colorCode='" + colorCode + '\'' + '}';
    }
}
