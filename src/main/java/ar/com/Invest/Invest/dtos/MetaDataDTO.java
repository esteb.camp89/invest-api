package ar.com.Invest.Invest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetaDataDTO {

    @JsonProperty(value = "1. Information")
    private String info;

    @JsonProperty(value = "2. Symbol")
    private String symbol;

    @JsonProperty(value = "3. Last Refreshed")
    private String lastRefreshed;

    @JsonProperty(value = "4. Output Size")
    private String outputSize;

    @JsonProperty(value = "5. Time Zone")
    private String timeZone;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getLastRefreshed() {
        return lastRefreshed;
    }

    public void setLastRefreshed(String lastRefreshed) {
        this.lastRefreshed = lastRefreshed;
    }

    public String getOutputSize() {
        return outputSize;
    }

    public void setOutputSize(String outputSize) {
        this.outputSize = outputSize;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Override
    public String toString() {
        return "MetaDataDTO{" +
                "info='" + info + '\'' +
                ", symbol='" + symbol + '\'' +
                ", lastRefreshed='" + lastRefreshed + '\'' +
                ", outputSize='" + outputSize + '\'' +
                ", timeZone='" + timeZone + '\'' +
                '}';
    }
}
