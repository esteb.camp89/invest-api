package ar.com.Invest.Invest.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Map;
import java.util.SortedMap;

public class ResponseAlphaDTO {

    @JsonProperty(value = "Meta Data")
    private MetaDataDTO metaData;

    @JsonProperty(value = "Time Series (Daily)")
    @JsonPropertyOrder(alphabetic=true)
    private Map<String,DailyDTO> timeSeriesDaily;

    public MetaDataDTO getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaDataDTO metaData) {
        this.metaData = metaData;
    }

    public Map<String, DailyDTO> getTimeSeriesDaily() {
        return timeSeriesDaily;
    }

    public void setTimeSeriesDaily(Map<String, DailyDTO> timeSeriesDaily) {
        this.timeSeriesDaily = timeSeriesDaily;
    }

    @Override
    public String toString() {
        return "ResponseAlphaDTO{" +
                "metaData=" + metaData +
                ", timeSeriesDaily=" + timeSeriesDaily +
                '}';
    }
}
