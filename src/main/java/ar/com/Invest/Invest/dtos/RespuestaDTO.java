package ar.com.Invest.Invest.dtos;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public class RespuestaDTO {

    private String symbol;

    @JsonProperty(value = "daily_actions")
    private RespuestaDailyDTO[] respuestaDailyDTO;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public RespuestaDailyDTO[] getRespuestaDailyDTO() {
        return respuestaDailyDTO;
    }

    public void setRespuestaDailyDTO(RespuestaDailyDTO[] respuestaDailyDTO) {
        this.respuestaDailyDTO = respuestaDailyDTO;
    }

    @Override
    public String toString() {
        return "RespuestaDTO{" + "symbol='" + symbol + '\'' + ", respuestaDailyDTO=" + Arrays.toString(respuestaDailyDTO) + '}';
    }
}
