/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.Invest.Invest.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseCustom {

    private ResponseEntity<Response> resHttp;

    public ResponseCustom(Object body) {
        Response res = new Response();
        res.setData(body);
        resHttp = new ResponseEntity(res, HttpStatus.OK);
    }

    public ResponseEntity getResponseOK() {
        resHttp.getBody().setStatus("0");
        resHttp.getBody().setMessage("");
        return resHttp;
    }

    public ResponseEntity getResponseFail(Integer code, String message) {
        resHttp.getBody().setStatus(code.toString());
        resHttp.getBody().setMessage(message);
        return resHttp;
    }
}
