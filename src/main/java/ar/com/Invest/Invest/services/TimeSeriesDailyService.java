package ar.com.Invest.Invest.services;

import ar.com.Invest.Invest.dtos.DailyDTO;
import ar.com.Invest.Invest.dtos.ResponseAlphaDTO;
import ar.com.Invest.Invest.dtos.RespuestaDTO;
import ar.com.Invest.Invest.dtos.RespuestaDailyDTO;
import ar.com.Invest.Invest.restinvoke.RestExternos;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

@Service
public class TimeSeriesDailyService {


    @Autowired
    private RestExternos restExternos;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TimeSeriesDailyService.class);

    public RespuestaDTO getTimes(String symbol) {
        LOG.debug("");
        RespuestaDTO respuestaDTO = null;
        ResponseAlphaDTO responseAlphaDTO = restExternos.getTimeSeriesDaily(symbol);
        LOG.debug("responseAlphaDTO: {}", responseAlphaDTO.toString());
        respuestaDTO = this.generarRespuesta(responseAlphaDTO.getTimeSeriesDaily());
        respuestaDTO.setSymbol(responseAlphaDTO.getMetaData().getSymbol());
        LOG.debug("respuestaDTO: {}", respuestaDTO.toString());
        return respuestaDTO;
    }

    private RespuestaDTO generarRespuesta(Map<String, DailyDTO> timeSeriesDaily) {
        RespuestaDTO respuestaDTO = new RespuestaDTO();

        Map<String, DailyDTO> newMap = new TreeMap<>(Collections.reverseOrder());
        newMap.putAll(timeSeriesDaily);
        RespuestaDailyDTO[] respuestaDailyDTOS = new RespuestaDailyDTO[newMap.size()];

        Iterator it1 = newMap.keySet().iterator();

        int i = 0;
        RespuestaDailyDTO respuestaDailyDTO = null;
        while (it1.hasNext()) {
            String key = (String) it1.next();
            DailyDTO dailyDTO = newMap.get(key);
            if (it1.hasNext()) {
                    String key2 = (String) it1.next();
                    DailyDTO dailyDTO2 = newMap.get(key2);
                    respuestaDailyDTO = this.calcular(dailyDTO, dailyDTO2);
                    respuestaDailyDTO.setValue(dailyDTO.getClose().toString());
                    respuestaDailyDTO.setPrevious(dailyDTO2.getClose().toString());
                    respuestaDailyDTO.setDate(key);
                    respuestaDailyDTOS[i] = respuestaDailyDTO;
                    i++;
            }
        }
        respuestaDTO.setRespuestaDailyDTO(respuestaDailyDTOS);
        return respuestaDTO;
    }

    private RespuestaDailyDTO calcular(DailyDTO dailyDTO, DailyDTO dailyDTO2) {
        RespuestaDailyDTO respuestaDailyDTO = new RespuestaDailyDTO();
        Double hoy = dailyDTO.getClose();
        Double ayer = dailyDTO2.getClose();

        Double resta = hoy - ayer;
        Double porcent = null;
        //si resta es mayor a 0 hubi
        if (resta > 0) {
            respuestaDailyDTO.setColorCode("green");
        } else {
            respuestaDailyDTO.setColorCode("red");
        }
        porcent = (resta * 100) / hoy;
        resta = (double) Math.round(resta * 100d) / 100d;
        porcent = (double) Math.round(porcent * 100d) / 100d;
        respuestaDailyDTO.setChangePercent(porcent.toString());
        respuestaDailyDTO.setChangeValue(resta.toString());
        return respuestaDailyDTO;
    }

}
