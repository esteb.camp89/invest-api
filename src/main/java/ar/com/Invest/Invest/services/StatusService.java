package ar.com.Invest.Invest.services;

import ar.com.Invest.Invest.repositories.StatusRepository;
import ar.com.Invest.Invest.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Timestamp;


@Service
public class StatusService {

    @Autowired
    private StatusRepository statusRepository;

    public Response getStatus() {
        Response response = new Response();
        Date date = statusRepository.findStatus();
        if (date != null) {
            response.setMessage("OK");
            response.setStatus("200");
            response.setAction("INVEST ACTIVO");
            response.setData("date: " + date);
        } else {
            response.setStatus("400");
            response.setMessage("BAD_REQUEST");
            response.setAction("INVEST ERROR");
            response.setData("date: " + null);
        }

        return response;
    }

}
