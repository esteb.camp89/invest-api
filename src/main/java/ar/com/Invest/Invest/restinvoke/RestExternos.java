/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.Invest.Invest.restinvoke;

import ar.com.Invest.Invest.config.RequestResponseLoggingInterceptor;
import ar.com.Invest.Invest.dtos.ResponseAlphaDTO;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.FieldNamingPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Collections;
import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class RestExternos {


    static final Logger LOG = LoggerFactory.getLogger(RestExternos.class);
    private Properties config = new Properties();

    public RestExternos() {
        try {
            String runtimeEnvironment = System.getenv().get("RUNTIME_ENVIRONMENT");
            //AGREGADO POR SI NO CONFIGURAN
            if(runtimeEnvironment==null||runtimeEnvironment.isEmpty()) {
                runtimeEnvironment = "DESARROLLO";
            }
            config.load(RestExternos.class.getResourceAsStream("/static/config"+runtimeEnvironment+".properties"));
        } catch (IOException ex) {
            LOG.debug("Error al cargar properties: {}",ex.getMessage());
        }
    }


    public ResponseAlphaDTO getTimeSeriesDaily(String Symbol) {
        LOG.debug("ingresa a getTimeSeriesDaily");
//        JsonNode response = null;
        ResponseAlphaDTO response = null;
        try {
            String uriAlpha = config.getProperty("urlAlpha");
            String privateApiKey = config.getProperty("privateApiKey");

            ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());

            RestTemplate restTemplate = new RestTemplate(factory);

            restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));

            LOG.debug("invoca restTemplate.postForObject() - Alpha TIME_SERIES_DAILY  -{}" , uriAlpha );

            final String uri = uriAlpha + "query?function=TIME_SERIES_DAILY&symbol="+Symbol+"&outputsize=compact&apikey="+privateApiKey;

            LOG.debug("invoca restTemplate.postForObject() - uri  -{}" , uri );
           // ObjectMapper objectMapper = new ObjectMapper();
            ResponseEntity<ResponseAlphaDTO> responseEntity =  restTemplate.exchange(
                    uri, HttpMethod.GET, null, ResponseAlphaDTO.class);

            response = responseEntity.getBody();
        } catch (Exception e) {
            LOG.error("Error api alpha  : {}", e.getMessage());
        }
        return response;
    }




}
