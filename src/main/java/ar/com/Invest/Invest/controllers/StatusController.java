/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.Invest.Invest.controllers;

import ar.com.Invest.Invest.config.RequestResponseLoggingInterceptor;
import ar.com.Invest.Invest.dtos.RespuestaDTO;
import ar.com.Invest.Invest.services.StatusService;
import ar.com.Invest.Invest.utils.Response;
import ar.com.Invest.Invest.utils.ResponseCustom;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/status")
public class StatusController {
    @Autowired
    private StatusService statusService;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(StatusController.class);

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity getStatus(@PathVariable("id") Long id) {
        Response response = statusService.getStatus();
        LOG.debug("Response: {}",response.toString());
            return new ResponseCustom(response).getResponseOK();
    }
}
