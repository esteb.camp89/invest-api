package ar.com.Invest.Invest.controllers;

import ar.com.Invest.Invest.dtos.ResponseAlphaDTO;
import ar.com.Invest.Invest.dtos.RespuestaDTO;
import ar.com.Invest.Invest.services.TimeSeriesDailyService;
import ar.com.Invest.Invest.utils.ResponseCustom;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
@RequestMapping("/times")
public class TimeSeriesDailyController {

    @Autowired
    private TimeSeriesDailyService timeSeriesDailyService;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TimeSeriesDailyController.class);

    @GetMapping(value = "/{symbol}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity getTimes(@PathVariable("symbol") String symbol) {
        LOG.info("endpoint: /times/{}",symbol );
        RespuestaDTO respuestaDTO = timeSeriesDailyService.getTimes(symbol);
        LOG.debug("responseAlpha: {}", respuestaDTO.toString());
        LOG.info("data: {}", respuestaDTO.toString());
        return new ResponseCustom(respuestaDTO).getResponseOK();
    }
}
