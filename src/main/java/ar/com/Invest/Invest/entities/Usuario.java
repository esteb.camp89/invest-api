package ar.com.Invest.Invest.entities;


import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity(name = "USUARIO")
public class Usuario {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.TABLE)
    private long id;

    private String username;

    private String password;

    public long getId() {
        return id;
    }

    @Size(max = 250)
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Size(max = 250)
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
