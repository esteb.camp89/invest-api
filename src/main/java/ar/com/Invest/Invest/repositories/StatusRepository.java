/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.Invest.Invest.repositories;


import ar.com.Invest.Invest.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 */
@Repository
public interface StatusRepository extends JpaRepository<Usuario, Long> {

    @Query(value = " SELECT CURRENT_DATE",nativeQuery = true)
    Date findStatus();
}
